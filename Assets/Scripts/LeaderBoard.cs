﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LeaderBoard : MonoBehaviour
{
    [SerializeField] private dreamloLeaderBoard myBoard;
    public List<dreamloLeaderBoard.Score> MyScores = new List<dreamloLeaderBoard.Score>();
    public GameObject linePrefab;
    public Transform board;
    public TextMeshProUGUI username;
    public GameObject addScoreUI;

    [Header("Positions")]
    public Vector3 positionDepart;
    public Vector3 spacing;

    private string playerName;
    private int totalScore;

    private Singleton gameManager;

    // Start is called before the first frame update
    void Start()
    {
        myBoard = dreamloLeaderBoard.GetSceneDreamloLeaderboard();
        gameManager = FindObjectOfType<Singleton>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void AddScore()
    {
        if (username.text.Length > 1)   // égal 1 si vide
        {
            playerName = username.text;
            totalScore = Mathf.RoundToInt(gameManager.GetScore());
            addScoreUI.SetActive(false);
            myBoard.AddScore(playerName, totalScore);
            StartCoroutine(GetRequest());
        }
    }

    private IEnumerator GetRequest()
    {
        while (!myBoard.Loaded)
        {
            yield return new WaitForSeconds(0.5f);
        }
        GetScores();
    }

    public void GetScores()
    {
        MyScores = myBoard.ToListHighToLow();
        int decalage = 0;
        foreach (var score in MyScores)
        {
            //Debug.Log(score.playerName + "/" + score.score);
            var ligne = Instantiate(linePrefab);
            ligne.transform.SetParent(board);
            ligne.transform.localPosition = positionDepart + (decalage*spacing);
            //ligne.transform.localPosition.y -= decalage*spacing;
            decalage += 1;
            ligne.transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = score.playerName;
            ligne.transform.GetChild(1).gameObject.GetComponent<TextMeshProUGUI>().text = score.score.ToString();
        }
    }

    public void AddScoreDebug()
    {
        myBoard.AddScore("test", 19);
        StartCoroutine(GetRequest());
    }

    public void exitGame() {
        Application.Quit();
    }
    
}
