﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public GameObject firstSound;
    public GameObject secondSound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaySecond(){
        firstSound.SetActive(false);
        secondSound.SetActive(true);
    }
}
