﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using UnityEngine.SceneManagement;

public class MenuInteractive_Manager : MonoBehaviour
{
    public LevelLoader LevelLoader;
    //public string[] actionChoice = {""}
    public GameObject player;
    public GameObject choices;
    public TextMeshProUGUI actionText;
    public GameObject SettingsUI;
    public GameObject ScanLight;
    //public Color ScanColor;

    private List<Transform> actionChoice = new List<Transform>();
    private float playerPosX;
    private int currentAction;
    public int inAction;

    private SoundManager soundManager;

    public Sprite[] spritesEquipement;
    public SpriteRenderer spriteEquipement;

    public int choiceEquipement = 0;
    public GameObject arrowEquipement;

    

    // Start is called before the first frame update
    void Start()
    {
        inAction = 0;
        foreach (Transform child in choices.transform)
        {
            actionChoice.Add(child);
        }

        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        //var ScanColor = ScanLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().color;
        //ScanColor = Color.red;
    }

    // Update is called once per frame
    void Update()
    {
        playerPosX = player.transform.position.x;
        CheckChange();

        if (Input.GetKeyDown("space"))
        {
            ActionStart();
        }

        if ( inAction == 1){
            if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown("d"))
            {
                choiceEquipement += 1;
                if (choiceEquipement >= spritesEquipement.Length) choiceEquipement = 0;
                UpdateChoice();
            }
            else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown("q"))
            {
                choiceEquipement -= 1;
                if (choiceEquipement < 0 ) choiceEquipement = spritesEquipement.Length - 1;
                UpdateChoice();
            }
        }
        
    }

    public void CheckChange(){
        for (int i = actionChoice.Count - 1 ; i >= 0; i--){
            var obj = actionChoice[i];
            if (playerPosX > obj.position.x - 2){
                actionText.text = obj.name;
                currentAction = i;
                return ;
            }
        }
    }

    void ActionStart(){
        
        var pos = actionChoice[currentAction].position;

        if (inAction != 0) inAction = 0;
        else inAction = currentAction;

        player.GetComponent<DroneMovement>().enabled = inAction == 0;
        player.GetComponent<SmoothMove>().destination = new Vector3 (pos.x,pos.y,player.transform.position.z);
        player.GetComponent<SmoothMove>().enabled = inAction != 0;
        arrowEquipement.SetActive(inAction == 1);


        //player.transform.position = new Vector3 (pos.x,pos.y,player.transform.position.z);

        switch (currentAction)
        {
            case 0:
                Play();
                break;
            case 1:
                Equipement();
                break;
            case 2:
                Settings();
                break;
            case 3:
                Exit();
                break;
        }
    }

    #region Actions
    // ----- ACTIONS ----- //

    void Play(){

        StartCoroutine(Alert());       
    }

    void Equipement(){
        UpdateChoice();
    }

    void UpdateChoice(){
        spriteEquipement.sprite = spritesEquipement[choiceEquipement];
    }

    void Settings(){
        if (inAction != 2){
            SettingsUI.SetActive(false);
            return;
        }
        SettingsUI.SetActive(true);
    }

    void Exit(){
        Application.Quit();
    }

    // ----- END ACTIONS ----- //
    #endregion
    IEnumerator Alert(){
        float timeWait = 0.5f;
        for (int i = 0 ; i < 3; i++){
            yield return new WaitForSeconds(timeWait);
            ScanLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().color = Color.yellow;
            yield return new WaitForSeconds(timeWait);
            ScanLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().color = Color.red;
        }
        yield return new WaitForSeconds(timeWait);
        ScanLight.GetComponent<UnityEngine.Experimental.Rendering.Universal.Light2D>().color = Color.yellow;
        soundManager.PlaySecond();
        PlayerPrefs.SetInt("idEquipement", choiceEquipement);
        LevelLoader.LoadMenuF();
        
    }
}
