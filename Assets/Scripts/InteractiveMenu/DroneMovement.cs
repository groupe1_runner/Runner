﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMovement : MonoBehaviour
{
    //public Rigidbody2D rb;
    public int speed = 5; 
    private bool flip = false;
    private SpriteRenderer sprite;
    // Start is called before the first frame update
    void Start()
    {
        sprite = this.GetComponent<SpriteRenderer>();
        //rb = this.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        var vDeplacement = Input.GetAxis("Vertical");
        var hDeplacement = Input.GetAxis("Horizontal");
        
        //rb.AddRelativeForce(0, 0, vDeplacement );
        transform.Translate(hDeplacement * Time.deltaTime * speed, vDeplacement * Time.deltaTime * speed, 0f);

        IsFlip(hDeplacement);
    }

    void IsFlip(float hDep){
        
        if (!flip && hDep < 0)
        {
            Flip();
        }
        if (flip && hDep > 0)
        {
            Flip();
        }
        
    }

    void Flip(){
        flip = !flip;
        sprite.flipX = flip;
    }
}
