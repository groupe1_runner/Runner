﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public GameObject GameoverUI;
    public GameObject player;
    public GameObject drones;
    public LevelLoader levelLoader;
    // Start is called before the first frame update
    void Start()
    {
        GameoverUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (player.transform.position.x<(drones.transform.position.x+1.2))
        {
            Launch();
        }
        
    }

    public void Launch()
    {
        GameoverUI.SetActive(true);
        GameoverUI.GetComponent<Animator>().SetTrigger("Start");
        levelLoader.transitionTime = 2;
        levelLoader.LoadMenuF();
    }

    private IEnumerator GameOverExit()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene("LeaderBoardScene");
    }
}
