﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Camera cam;
    private Animator animator;

    private float vSpeed;
    private float hSpeed;
    private int speedChange;
    private float gravityChange;
    public bool canMove;

    private float distanceTraveled;
    private bool isSliding;
    private bool wasSliding;

    public KeyCode SlidingKey = KeyCode.S;

    public Rigidbody2D rb;

    public Singleton gameManager;

    public GameObject runCollider;
    public GameObject slideCollider;
    public GameObject runWallCollider;
    public GameObject slideWallCollider;
    public GameObject groundCheck;

    void Start(){
        canMove = true;
        animator = this.GetComponent<Animator>();
        Singleton.Instance.loadFile();
        gameManager = Singleton.Instance;

        vSpeed = gameManager.vSpeed;
        hSpeed = gameManager.hSpeed;
        speedChange = gameManager.speedChange;
        gravityChange = gameManager.gravityChange;
    }


    void Update()
    {
        
        
        if (IsGrounded()){
            if (Input.GetKeyDown("space")){
                rb.velocity = Vector2.up * vSpeed;
                cam.GetComponent<CameraFollow>().UnZoom();
            }
            //cam.GetComponent<CameraFollow>().Zoom();       

        }
        if (rb.velocity.y < 0){
            cam.GetComponent<CameraFollow>().Zoom();
        }


        if (Input.GetKey(SlidingKey))
        {
            isSliding = true;
            SwitchColliders(isSliding);
        }

        if (Input.GetKeyUp(SlidingKey))
        {
            isSliding = false;
            SwitchColliders(isSliding);
        }

        animator.SetBool("IsSliding", isSliding);

        // can move 
        canMove = !CanMove(isSliding);
        if (canMove){
            transform.Translate(hSpeed * Time.deltaTime, 0f, 0f);
            distanceTraveled = transform.localPosition.x;
        }

        animator.SetBool("IsJumping",!IsGrounded());
    }

    private void SwitchColliders(bool b)
    {
        //Debug.Log("switching colliders");
        runCollider.SetActive(!b);
        runWallCollider.SetActive(!b);
        slideCollider.SetActive(b);
        slideWallCollider.SetActive(b);
    }

    private bool CanMove(bool isSliding)
    {
        if (isSliding)
            return slideWallCollider.GetComponent<GroundCheck>().isGrounded;
        else
            return runWallCollider.GetComponent<GroundCheck>().isGrounded;
    }

    private bool IsGrounded()
    {
        
        return groundCheck.GetComponent<GroundCheck>().isGrounded;
    }

    public void Fast()
    {
        vSpeed *= speedChange;
    }

    public void Slow()
    {
        if (vSpeed > speedChange)
        {
            vSpeed /= speedChange;
        }
    }

    public void LowGravity()
    {
        rb.mass -= gravityChange;
    }

    public void HighGravity()
    {
        rb.mass += gravityChange;
    }
}
