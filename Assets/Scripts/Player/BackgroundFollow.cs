﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundFollow : MonoBehaviour
{
    public Transform target;



    void Update()
    {
        // Define a target position above and behind the target transform
        float x  = target.transform.position.x;

        // Smoothly move the camera towards that target position
        SetTransformX(x);
    }

    void SetTransformX(float n){
        transform.position = new Vector3(n, transform.position.y, transform.position.z);
    }
}
