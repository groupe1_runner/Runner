﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerEquipement : MonoBehaviour
{
    [Header("Equipment cooldown")]
    public float equipCooldown = 3.0f;
    public float tpCoolDown = 10f;

    [Header("UI prefabs")]
    public StackBar stackBar;
    public EquipmentBar[] equipBar;
    public PassiveEquipementBar passiveEquipBars;

    [Header("Colliders")]
    public GameObject[] playerColliders;

    [Header("Equip values")]
    public float dashSpeed;
    public float doubleSautForce;

    [Header("TP settings")]
    public float deadZoneHeigth;
    public float tpResetHeightPos;
    public float tpHorizontalBonus;

    [Header("GameOver")]
    //public GameObject GameoverUI;

    #region private variables

    private Vector2 playerPos;
    private bool isPlayerInDeadZone = false;
    private Animator playerAnimator;
    
    #endregion

    void Start()
    {    
        playerAnimator = this.GetComponent<Animator>();
    }

    void Update()
    {
        UpdatePlayerPos();
        CheckIfPlayerInDeadZone();
    }    

    private void UpdatePlayerPos()
    {
        playerPos = gameObject.transform.position;
    }


    void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.CompareTag("StackUp"))
        {
            stackBar.AddStacks();
            Destroy(collision.gameObject);
        }

        if (collision.CompareTag("Energy"))
        {
            passiveEquipBars.LoadCharge();
            Destroy(collision.gameObject);
        }        
    }

    #region POWERS FUNCTIONS ! \('O')/
    public void Dash()
    {
        Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();

        rb.velocity = Vector2.zero;
        rb.AddForce(Vector2.right * dashSpeed, ForceMode2D.Impulse);
    }

    public void DoubleSaut()
    {
        Rigidbody2D rb = gameObject.GetComponent<Rigidbody2D>();

        rb.velocity = Vector2.zero;
        rb.AddForce(Vector2.up * doubleSautForce, ForceMode2D.Impulse);
    }

    public void Glitch()
    {
        GhostMode();
        StartCoroutine(ResetGhostMode());        
    }

    private IEnumerator ResetGhostMode()
    {
        yield return new WaitForSeconds(1f);
        HumanMode();
    }

    private void GhostMode()
    {
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;

        foreach (GameObject colliders in playerColliders)
        {
            // Permet de passer à travers le collider
            colliders.SetActive(false);
        }
        /* VISUAL 
        Color ghostColor = Color.white;
        ghostColor.a = 0.33f;

        gameObject.GetComponent<SpriteRenderer>().color = ghostColor;
        */
        Debug.Log("Start Ghost");
        playerAnimator.SetBool("IsGlitching", true);
        //TRIGGER GHOST ANIMATION
    }

    private void HumanMode()
    {
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        gameObject.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

        foreach (GameObject colliders in playerColliders)
        {
            // Permet de passer à travers le collider
            colliders.SetActive(true);
        }
        /*
        Color humanColor = Color.white;
        humanColor.a = 1;

        gameObject.GetComponent<SpriteRenderer>().color = humanColor;  
        */
        Debug.Log("Stop Ghost");
        playerAnimator.SetBool("IsGlitching", false);
        //TRIGGER HUMAN ANIMATION
    }

    #endregion

    private void CheckIfPlayerInDeadZone()
    {
        isPlayerInDeadZone = (playerPos.y < deadZoneHeigth);

        if (isPlayerInDeadZone)
        {
            bool isReady = passiveEquipBars.IsEquipementReady;
            if (isReady)
            {
                passiveEquipBars.UseCharge();
                Vector3 newPlayerPos = new Vector3(playerPos.x + tpHorizontalBonus, tpResetHeightPos);
                transform.position = newPlayerPos;
            }
            else
            {
                GetComponent<GameOver>().Launch();
            }
        }
    }


    void OnDrawGizmosSelected()
    {

        Vector3 deadZoneA = new Vector3(playerPos.x - 50f, deadZoneHeigth, 0.0f);
        Vector3 deadZoneB = new Vector3(playerPos.x + 50f, deadZoneHeigth, 0.0f);

        Vector3 tpPoint = new Vector3(playerPos.x + tpHorizontalBonus, tpResetHeightPos, 0.0f);

        // Draws a red line based on player pos to mark the dead zone
        Gizmos.color = Color.red;
        Gizmos.DrawLine(deadZoneA, deadZoneB);

        // Draws yellow sphere to mark TP position
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(tpPoint, 0.5f);
    }
}