﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
     
public class CameraFollow : MonoBehaviour
{
    public Transform target;
    public float smoothTime = 0.5F;
    private Vector3 velocity = Vector3.zero;
    private Vector3 offSet;
    public Vector3 zoom;
    public Vector3 unZoom;

    void Start(){
        offSet = zoom;
    }

    void Update()
    {
        // Define a target position above and behind the target transform
        Vector3 targetPosition = target.TransformPoint(offSet);

        // Smoothly move the camera towards that target position
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    }

    public void UnZoom(){
        offSet = unZoom;
    }

    public void Zoom(){
        offSet = zoom;
    }
}