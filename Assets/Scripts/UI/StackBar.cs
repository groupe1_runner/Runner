﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StackBar : MonoBehaviour
{
    
    public Image clockStackImage;
    public TextMeshProUGUI stackText;
    public TextMeshProUGUI stackMaxText;
    public int maxStacks = 5;
    public float waitTime = 5f;

    private int stacksSize = 2;

    void Start()
    {

        AvailableStacks = stacksSize;
        UpdateStackText();
    }


    void Update()
    {
        if (AvailableStacks < stacksSize)
        {
            loadStacks();
        }
    }


    private void loadStacks()
    {
        if (clockStackImage.fillAmount == 1.0f)
        {
            clockStackImage.fillAmount = 0f;
        }
        else
        {
            clockStackImage.fillAmount += 1.0f / waitTime * Time.deltaTime;
        }

        if (clockStackImage.fillAmount == 1.0f)
        {
            AvailableStacks++;
            UpdateStackText();

            clockStackImage.fillAmount = 0f;
        }
    }

    public void AddStacks()
    {
        if(stacksSize < maxStacks)
        {
            stacksSize++;
            stacksSize = Mathf.Clamp(stacksSize, 1, maxStacks);
        }
        else
        {
            if(AvailableStacks < stacksSize) AvailableStacks++;

            if(AvailableStacks == stacksSize)
            {
                if(clockStackImage.fillAmount > 0f)
                {
                    clockStackImage.fillAmount = 0f;
                }
            }
        }

        UpdateStackText();
    }

    public void UseStack()
    {
        AvailableStacks--;
        AvailableStacks = Mathf.Clamp(AvailableStacks, 0, stacksSize);
        UpdateStackText();
        clockStackImage.fillAmount = 0;
    }

    public int AvailableStacks { get; private set; }


    private void UpdateStackText()
    {
        stackText.text = $"{AvailableStacks}";
        stackMaxText.text = $"{stacksSize}";
    }

    private void PrintMessage(string msg)
    {
        print($"{msg}: {AvailableStacks}/{stacksSize}");
    }

}
