﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EquipmentBar : MonoBehaviour
{
    #region PUBLIC VARIABLES 
    public KeyCode key;

    [Range(1, 10f)]
    public float coolDownTime = 3.0f;    

    [Range(0, 2)]
    public int equipSelected = 2;
    public string[] equipName = { "DoubleSaut", "Dash", "Glitch", };

    public Image iconImage;
    public Sprite[] iconSprites;

    public Image clockStackImage;
    public TextMeshProUGUI textMesh;
    
    public StackBar stackCounter;
    #endregion

    //public enum Equipment { Doublesaut, Dash, Glitch }
    //public Equipment selectEquipment;

    private bool stacks;
    private float timer;
    private string keyString;

    private GameObject player;
    private PlayerEquipement playerEquipement;
    


    void Start()
    {
        SelectEquipment(PlayerPrefs.GetInt("idEquipement"));

        player = GameObject.FindGameObjectWithTag("Player");
        
        playerEquipement = player.GetComponent<PlayerEquipement>();

        keyString = key.ToString();        

        textMesh.text = keyString;
        timer = coolDownTime;

        SetIcon();
    }

    void Update()
    {
        if (IsEquipmentLaunched)
        {
            LoadStacks();
        }

        //NEW
        if (Input.GetKeyDown(key))
        {
            bool stacks = (stackCounter.AvailableStacks > 0);
            if (!IsEquipmentLaunched && stacks)
            {
                UseEquipement();
            }
        }
    }
    

    private void SetIcon()
    {
        string e = equipName[equipSelected];
        switch (e)
        {
            case "DoubleSaut":
                iconImage.sprite = iconSprites[equipSelected ];
                break;
            case "Dash":
                iconImage.sprite = iconSprites[equipSelected ];
                break;
            case "Glitch":
                iconImage.sprite = iconSprites[equipSelected ];
                break;
        }
    }

    private void UseEquipement()
    {
        stackCounter.UseStack();
        IsEquipmentLaunched = true;

        string e = equipName[equipSelected];
        switch (e) 
        {
            case "DoubleSaut":
                playerEquipement.DoubleSaut();                
                break;
            case "Dash":
                playerEquipement.Dash();
                break;
            case "Glitch":
                playerEquipement.Glitch();
                break;
        }
    }


    private void LoadStacks()
    {
        /*if (clockStackImage.fillAmount == 1.0f)
        {
            clockStackImage.fillAmount = 0f;
            timer = coolDownTime;
        }
        else*/
        if(clockStackImage.fillAmount < 1.0f)
        {
            clockStackImage.fillAmount += 1.0f / coolDownTime * Time.deltaTime;  //* 1.0f / coolDownTime * Time.deltaTime;*//
            timer -= Time.deltaTime;
            timer = Mathf.Clamp(timer, 0f, coolDownTime);
            textMesh.text = $"{timer:n1}";
        }

        if (clockStackImage.fillAmount == 1.0f)
        {
            clockStackImage.fillAmount = 0f;
            textMesh.text = keyString;
            IsEquipmentLaunched = false;
            timer = coolDownTime;
        }
    }



    #region PUBLIC FUNCTIONS //
    public void SelectEquipment(int e)
    {
        equipSelected = e;
    }

    public bool IsEquipmentLaunched { get; set; } = false;

    #endregion

}
