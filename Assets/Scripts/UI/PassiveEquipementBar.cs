﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class PassiveEquipementBar : MonoBehaviour
{
    [Header("UI prefab childs")]
    public Slider loadSlider;
    public TextMeshProUGUI text;

    public int sliderMaxValue = 10;

    [Header("energy balls to load")]
    [Range(1f, 10f)]
    public int energyValue = 2;

    private float chargeValue;

    void Start()
    {
        loadSlider.wholeNumbers = true;
        loadSlider.maxValue = sliderMaxValue;
        loadSlider.value = sliderMaxValue;
        chargeValue = sliderMaxValue;
    }


    public void LoadCharge()
    {
        if (chargeValue < sliderMaxValue)
        {
            chargeValue += energyValue;
            loadSlider.value = Mathf.Clamp(chargeValue, 0, sliderMaxValue);
            text.text = $"{chargeValue * 10}% LOADED";            
        }

        if (chargeValue == sliderMaxValue)
        {
            IsEquipementReady = true;
            text.text = "READY";
        }
    }

    public void UseCharge()
    {
        chargeValue = 0;
        loadSlider.value = chargeValue;
        text.text = $"Energy critical";
        IsEquipementReady = false;
    }


    public bool IsEquipementReady { get; set; } = true;

    
}
