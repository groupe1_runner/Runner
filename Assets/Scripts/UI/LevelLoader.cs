﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class LevelLoader : MonoBehaviour
{
    public int sceneTarget;
    public bool goToNextLvl;
    public GameObject blackScreen; 
    public float transitionTime = 2f;

    public void Update(){
        if (goToNextLvl && Input.GetKeyDown("space"))
        {
            LoadMenuF();
        }
    }

    public void LoadMenuF(){
        StartCoroutine(LoadMenu());
    }

    IEnumerator LoadMenu(){
        // Play animation
        blackScreen.GetComponent<Animator>().SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(sceneTarget);
    }
}
