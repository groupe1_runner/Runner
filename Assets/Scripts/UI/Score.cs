﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    private int score;
    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = $"Score:{score}";
    }

    // Update is called once per frame
    void Update()
    {
        score = Mathf.RoundToInt(gameObject.transform.localPosition.x);
        if (score>=0)
        {
            scoreText.text = $"Score:{score}";
        }
    }
}
