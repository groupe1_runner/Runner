﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour
{
    private string startSceneName = "Runner";
    public GameObject titleScreen;
    public GameObject objectsScreen;
    public GameObject settingsScreen;

    void Start()
    {
        ViewTitle();
    }

    
    /*
    public void LoadMenu(){
        blackScreen
        
        //CrossFadeAlpha(0.1f, 2.0f, false);
        
        
    }
    */

    

    public void NewGame()
    {
        objectsScreen.SetActive(true);
        titleScreen.SetActive(false);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(startSceneName);
    }

    public void ViewSettings()
    {
        titleScreen.SetActive(false);
        settingsScreen.SetActive(true);
    }

    public void ViewTitle()
    {
        objectsScreen.SetActive(false);
        settingsScreen.SetActive(false);
        titleScreen.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
