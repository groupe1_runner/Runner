﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton : MonoBehaviour
{
    public float vSpeed;
    public float hSpeed;
    public int speedChange;
    public float gravityChange;

    private float score;

     // Static singleton instance
    private static Singleton instance;
     
    // Static singleton property
    public static Singleton Instance
    {
       // ajout ET création du composant à un GameObject nommé "SingletonHolder" 
      get { return instance ?? (instance = new GameObject("GameManager").AddComponent<Singleton>());} 
      private set{ instance = value;} 
    }

    void Awake(){
        DontDestroyOnLoad(gameObject);//le GameObject qui porte ce script ne sera pas détruit
    }
 
    //méthode appelée par les "clients" de cette classe 
    public void loadFile(){ 
        vSpeed = 7.5f;
        hSpeed = 5.5f;
        speedChange = 2;
        gravityChange = 0.1f;
    }

    public void SetScore(float sc)
    {
        score = sc;
    }

    public float GetScore()
    {
        return score;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Translate(5f * Time.deltaTime, 0f, 0f);
    }
}


