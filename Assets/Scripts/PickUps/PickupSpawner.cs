﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour
{

    [Header("Pickup to spawn")]
    public GameObject PickupPrefab;
    public float SpawnDelayMin = 1;
    public float SpawnDelayMax = 5;

    [Header("Player GameObject")]
    public GameObject player;
    private Vector3 playerPos;


    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnEnergy());
    }

    // Update is called once per frame
    void Update()
    {
        playerPos = player.transform.position;
    }


    private IEnumerator SpawnEnergy()
    {
        float nbSec = Random.Range(SpawnDelayMin, SpawnDelayMax + 1);
        yield return new WaitForSeconds(nbSec);
        Vector3 position = new Vector3(playerPos.x + 10, 10f, 0f);
        Quaternion rotation = Quaternion.identity;
        Instantiate(PickupPrefab, position, rotation);
        StartCoroutine(SpawnEnergy());
    }

}
