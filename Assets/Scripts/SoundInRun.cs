﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundInRun : MonoBehaviour
{
    public AudioSource gameLaunch;
    public AudioSource gameLoop;

    public int playAfter = 30;

    // Start is called before the first frame update
    void Awake()
    {
        gameLaunch.Play();
        gameLoop.PlayDelayed(playAfter);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
