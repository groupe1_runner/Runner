﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObstacle : MonoBehaviour
{
    public Transform[] Obstacles;
    public Transform minSpawner;
    public Transform maxSpawner;

    public int obsFrenquency ; // 1 / obsFrequency

    // Start is called before the first frame update
    void Start()
    {
        bool didSpawn = (1 == Random.Range(0, obsFrenquency ));
        if (didSpawn) SpawnObs() ;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameObject SpawnObs(){
        int iObs = Random.Range(0, Obstacles.Length );
        GameObject obj = Instantiate(Obstacles[iObs]).gameObject;

        float xObs = Random.Range(minSpawner.position.x, maxSpawner.position.x );
        //obj.transform.position.x = xObs;
        SetTransformX(obj,xObs);
        obj.transform.SetParent(this.gameObject.transform);
        return obj;
    }

    void SetTransformX(GameObject obj, float n){
        obj.transform.position = new Vector3(n, minSpawner.position.y, obj.transform.position.z);
    }
}
