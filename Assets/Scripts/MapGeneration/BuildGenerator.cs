﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildGenerator : MonoBehaviour
{
    private List<List<Sprite>> buildingSkins = new List<List<Sprite>>();

    public List<Sprite> skinA;
    public List<Sprite> skinB;
    public List<Sprite> skinC;

    private int ntypeBuild = 0;

    public Transform buildingPrefab;


    public Singleton gameManager;
    
    public int nbOfObjects;

    public Vector3 startPosition;
    private Vector3 spawnPoint;
    private Vector3 spawnPosition;

    private int buildPosed;

    public float maxRange;
    private Vector3 decalage;

    float xdistRand = 0;
    float ydistRand = 0;


    private Queue<GameObject> buildQueue ;

    // Start is called before the first frame update
    public void Start()
    {
        buildingSkins.Add(skinA);
        buildingSkins.Add(skinB);
        buildingSkins.Add(skinC);

        buildPosed = 0;
        int buildLimite = 15;
        buildQueue = new Queue<GameObject>(5);
        gameManager = Singleton.Instance;

        spawnPosition = startPosition;
        spawnPoint = spawnPosition;

        for (int i = 0; i < buildLimite ; i ++)
            SpawnNext(true);
    }

    // Update is called once per frame
    void Update()
    {
        
        if (spawnPoint.x < Runner.distanceTraveled + 30)
        {
            SpawnNext(false);
        }
        
    }

    // How many build u want to spawn
    private void SpawnNext(bool init)
    {
        SpawnLongBuilding(init);
    }

    private void SpawnLongBuilding(bool init){

        int nBuilding = Random.Range(1, 5);
        Debug.Log(buildingSkins);

        ntypeBuild  =  Random.Range(0, buildingSkins.Count);

        SpawnSingleBuilding(init, false);
        for (int i = 0 ; i < nBuilding ; i++){
            SpawnSingleBuilding(init, true);
        }

    }

    private void SpawnSingleBuilding(bool init , bool longBuild){
        GameObject building = GenerateBuilding();
        buildQueue.Enqueue(building);

        building.transform.position = spawnPosition;

        

        if (!longBuild) {
            
            xdistRand = Random.Range(0, maxRange);
            ydistRand = Random.Range(0, maxRange - xdistRand);
            decalage = new Vector3 (building.transform.position.x + xdistRand,   building.transform.position.y + ydistRand ,    0);
            building.transform.position = decalage;
            spawnPosition.x += building.GetComponent<SpriteRenderer>().bounds.size.x + xdistRand;
            spawnPoint.x += building.GetComponent<SpriteRenderer>().bounds.size.x + xdistRand;
        }
        else{
            decalage = new Vector3 (building.transform.position.x,   building.transform.position.y + ydistRand ,    0);
            building.transform.position = decalage;
            spawnPosition.x += building.GetComponent<SpriteRenderer>().bounds.size.x ;
            spawnPoint.x += building.GetComponent<SpriteRenderer>().bounds.size.x;
        }
        

        

        if (!init){
            GameObject t = buildQueue.Dequeue();
            Destroy(t);
        } 
    }

    private GameObject GenerateBuilding()
    {
        
        buildPosed += 1;
        int numBuild = Random.Range(0,buildingSkins[ntypeBuild].Count);
        Sprite skin = buildingSkins[ntypeBuild][numBuild];
        buildingPrefab.GetComponent<SpriteRenderer>().sprite = skin;

        return Instantiate(buildingPrefab).gameObject;
    }


}
