﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{

    public string[] typeList = { "Stack up", "Extra life", "Dash", "Double saut" };
    public int selectType;

    private string type;

    // Start is called before the first frame update
    void Start()
    {
        type = typeList[selectType];
    }

    // Update is called once per frame
    void Update()
    {

    }

    public string PickUpType
    {
        get
        {
            return type;
        }
    }

    public void ChangeType(int t)
    {
        selectType = t;
    }
    
}
